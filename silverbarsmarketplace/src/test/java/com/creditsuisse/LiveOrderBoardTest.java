package com.creditsuisse;

import org.junit.Test;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;

import static org.junit.Assert.assertEquals;

public class LiveOrderBoardTest {
    @Test
    public void testThatNewOrderRegistrationUpdatesLiveBoard() {
        LiveOrderBoard liveOrderBoard = new LiveOrderBoard();
        liveOrderBoard.registerOrder(new Order("user1", 3.5, new BigDecimal(303), OrderType.SELL));
        assertEquals(Collections.singletonList("3.5 kg for £303"), liveOrderBoard.getSellSummary());
    }

    @Test
    public void testThatOrderCancellationRemovesOrderFromLiveBoard() {
        LiveOrderBoard liveOrderBoard = new LiveOrderBoard();
        liveOrderBoard.registerOrder(new Order("user1", 3.5, new BigDecimal(303), OrderType.SELL));
        liveOrderBoard.cancelOrder(new Order("user1", 3.5, new BigDecimal(303), OrderType.SELL));
        assertEquals(Collections.emptyList(), liveOrderBoard.getSellSummary());
    }

    @Test
    public void testThatLiveBoardDisplaysSellSummaryInAscendingOrder() {
        LiveOrderBoard liveOrderBoard = new LiveOrderBoard();
        liveOrderBoard.registerOrder(new Order("user1", 3.5, new BigDecimal(306), OrderType.SELL));
        liveOrderBoard.registerOrder(new Order("user2", 1.2, new BigDecimal(310), OrderType.SELL));
        liveOrderBoard.registerOrder(new Order("user3", 1.5, new BigDecimal(307), OrderType.SELL));
        liveOrderBoard.registerOrder(new Order("user4", 2.0, new BigDecimal(306), OrderType.SELL));
        assertEquals(Arrays.asList("5.5 kg for £306", "1.5 kg for £307", "1.2 kg for £310"), liveOrderBoard.getSellSummary());
    }

    @Test
    public void testThatLiveBoardDisplaysBuySummaryInDescendingOrder() {
        LiveOrderBoard liveOrderBoard = new LiveOrderBoard();
        liveOrderBoard.registerOrder(new Order("user1", 3.5, new BigDecimal(306), OrderType.BUY));
        liveOrderBoard.registerOrder(new Order("user2", 1.2, new BigDecimal(310), OrderType.BUY));
        liveOrderBoard.registerOrder(new Order("user3", 1.5, new BigDecimal(307), OrderType.BUY));
        liveOrderBoard.registerOrder(new Order("user4", 2.0, new BigDecimal(306), OrderType.BUY));
        assertEquals(Arrays.asList("1.2 kg for £310", "1.5 kg for £307", "5.5 kg for £306"), liveOrderBoard.getBuySummary());
    }

    @Test
    public void testThatLiveBoardProvidesOnlyBuySummaryExclusivelyWhenRequested() {
        LiveOrderBoard liveOrderBoard = new LiveOrderBoard();
        liveOrderBoard.registerOrder(new Order("user1", 3.5, new BigDecimal(306), OrderType.BUY));
        liveOrderBoard.registerOrder(new Order("user2", 1.2, new BigDecimal(310), OrderType.BUY));
        liveOrderBoard.registerOrder(new Order("user3", 1.5, new BigDecimal(307), OrderType.BUY));
        liveOrderBoard.registerOrder(new Order("user4", 2.0, new BigDecimal(306), OrderType.BUY));

        liveOrderBoard.registerOrder(new Order("user1", 3.5, new BigDecimal(306), OrderType.SELL));
        liveOrderBoard.registerOrder(new Order("user2", 1.2, new BigDecimal(310), OrderType.SELL));
        liveOrderBoard.registerOrder(new Order("user3", 1.5, new BigDecimal(307), OrderType.SELL));
        liveOrderBoard.registerOrder(new Order("user4", 2.0, new BigDecimal(306), OrderType.SELL));


        assertEquals(Arrays.asList("1.2 kg for £310", "1.5 kg for £307", "5.5 kg for £306"), liveOrderBoard.getBuySummary());
    }

    @Test
    public void testThatLiveBoardProvidesOnlySellSummaryExclusivelyWhenRequested() {
        LiveOrderBoard liveOrderBoard = new LiveOrderBoard();
        liveOrderBoard.registerOrder(new Order("user1", 3.5, new BigDecimal(306), OrderType.BUY));
        liveOrderBoard.registerOrder(new Order("user2", 1.2, new BigDecimal(310), OrderType.BUY));
        liveOrderBoard.registerOrder(new Order("user3", 1.5, new BigDecimal(307), OrderType.BUY));
        liveOrderBoard.registerOrder(new Order("user4", 2.0, new BigDecimal(306), OrderType.BUY));

        liveOrderBoard.registerOrder(new Order("user1", 3.5, new BigDecimal(306), OrderType.SELL));
        liveOrderBoard.registerOrder(new Order("user2", 1.2, new BigDecimal(310), OrderType.SELL));
        liveOrderBoard.registerOrder(new Order("user3", 1.5, new BigDecimal(307), OrderType.SELL));
        liveOrderBoard.registerOrder(new Order("user4", 2.0, new BigDecimal(306), OrderType.SELL));

        assertEquals(Arrays.asList("5.5 kg for £306", "1.5 kg for £307", "1.2 kg for £310"), liveOrderBoard.getSellSummary());

    }
}
