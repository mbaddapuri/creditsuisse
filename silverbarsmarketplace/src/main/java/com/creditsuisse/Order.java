package com.creditsuisse;

import java.math.BigDecimal;

public class Order {
    private final String user;
    private final double orderQuantity;
    private final BigDecimal pricePerKg;
    private final OrderType orderType;

    public Order(String user, double orderQuantity, BigDecimal pricePerKg, OrderType orderType) {
        this.user = user;
        this.orderQuantity = orderQuantity;
        this.pricePerKg = pricePerKg;
        this.orderType = orderType;
    }

    public String getUser() {
        return user;
    }

    public double getOrderQuantity() {
        return orderQuantity;
    }

    public BigDecimal getPricePerKg() {
        return pricePerKg;
    }

    public OrderType getOrderType() {
        return orderType;
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + user.hashCode();
        long orderLongBits = Double.doubleToLongBits(orderQuantity);
        result = 31 * result + (int) (orderLongBits ^ (orderLongBits >>> 32));
        result = 31 * result + pricePerKg.hashCode();
        result = 31 * result + orderType.hashCode();

        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;

        if (obj.getClass() != this.getClass()) return false;

        Order other = (Order) obj;

        return this.user.equals(other.user)
                && this.pricePerKg.equals(other.pricePerKg)
                && this.orderQuantity == other.orderQuantity
                && this.orderType == other.orderType;

    }
}
