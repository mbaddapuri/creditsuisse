package com.creditsuisse;

public enum OrderType {
    BUY, SELL
}
