package com.creditsuisse;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.stream.Collectors;

/**
 * This class allows the users to register an order, cancel an order and display the list of orders
 */
public class LiveOrderBoard {
    private final Collection<Order> orders = new ConcurrentLinkedQueue<>();

    /**
     * registers an order with LiveOrderBoard
     * @param order Order to register
     */
    public void registerOrder(Order order) {
        orders.add(order);
    }

    /**
     * Cancels an order from LiveOrderBoard
     * @param order Order to cancel
     */
    public void cancelOrder(Order order) {
        orders.remove(order);
    }

    /**
     * Returns All the Sell Orders ordered by order price
     * Imagine we have received the following orders:
     * - a) SELL: 3.5 kg for £306 [user1]
     * - b) SELL: 1.2 kg for £310 [user2]
     * - c) SELL: 1.5 kg for £307 [user3]
     * - d) SELL: 2.0 kg for £306 [user4]
     *  ‘Live Order Board’ will provide us the following summary information:
     * - 5.5 kg for £306 // order a + order d
     * - 1.5 kg for £307 // order c
     * - 1.2 kg for £310 // order b
     * orders for the same price will be merged together (even when they are from different users).
     * In this case it can be seen that order a) and d) were for the same amount (£306)
     * and this is why only their sum (5.5 kg) is displayed (for £306) and not the individual orders (3.5 kg and 2.0
     * kg).
     *
     * The last thing to note is that for SELL orders the orders with lowest prices are displayed first.
     *
     * @return Returns Sell Orders Summary
     */
    public List<String> getSellSummary() {
        Map<BigDecimal, Double> groupedOrders = orders.stream()
                .filter(e->e.getOrderType()==OrderType.SELL)
                .collect(Collectors.groupingBy(
                        Order::getPricePerKg,
                        Collectors.summingDouble(Order::getOrderQuantity)));

        return groupedOrders.entrySet()
                .stream()
                .sorted(Comparator.comparing(Map.Entry::getKey))
                .map(e-> e.getValue() + " kg for £" + e.getKey()).collect(Collectors.toList());
    }


    /**
     * Returns All the Buy Orders ordered by order price
     * Imagine we have received the following orders:
     * - a) SELL: 3.5 kg for £306 [user1]
     * - b) SELL: 1.2 kg for £310 [user2]
     * - c) SELL: 1.5 kg for £307 [user3]
     * - d) SELL: 2.0 kg for £306 [user4]
     *  ‘Live Order Board’ will provide us the following summary information:
     * - 1.2 kg for £310 // order b
     * - 1.5 kg for £307 // order c
     * - 5.5 kg for £306 // order a + order d
     * orders for the same price will be merged together (even when they are from different users).
     * In this case it can be seen that order a) and d) were for the same amount (£306)
     * and this is why only their sum (5.5 kg) is displayed (for £306) and not the individual orders (3.5 kg and 2.0
     * kg).
     *
     * The last thing to note is that for BUY orders the orders with highest prices are displayed first.
     *
     * @return Returns Buy Orders Summary
     */
    public List<String> getBuySummary() {
        Map<BigDecimal, Double> groupedOrders = orders.stream()
                .filter(e->e.getOrderType()==OrderType.BUY)
                .collect(Collectors.groupingBy(
                        Order::getPricePerKg,
                        Collectors.summingDouble(Order::getOrderQuantity)));

        return groupedOrders.entrySet()
                .stream()
                .sorted(Comparator.comparing(Map.Entry::getKey, Comparator.reverseOrder()))
                .map(e-> e.getValue() + " kg for £" + e.getKey()).collect(Collectors.toList());
    }

}
