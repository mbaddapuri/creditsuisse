
Notes/Assumptions:
1. Assumed that returning the summary information as a List of Strings is acceptable behaviour as per instructions
2. Due to lack of instructions on separate summary for BUY and SELL, I have provided two different summary methods - one each for BUY and SELL
3. Assumed weight in kg and £ as currency are static for the purpose of this test
4. Used ConcurrentLinkedQueue as container for the orders - Considered an efficient, thread-safe and concurrent collection for this use case